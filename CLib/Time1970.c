#include "Time1970.h"
#include <stdio.h>

#define sec_in_day      ((uint32_t)60*60*24)
#define sec_in_year     ((uint32_t)60*60*24*365)
#define StartOfTime     1970            // 2000
#define StartDay        4               // 6 Saturday

const uint8_t day_in_month[12]=
{
    31,28,31,30,31,30,31,31,30,31,30,31
};
const uint8_t nDayOfWeek[7][4]=
{
    "Sun\0","Mon\0","Tue\0","Wed\0","Thu\0","Fri\0","Sat\0",
};
const uint8_t Month[12][4]=
{
    "Jan\0","Feb\0","Mar\0","Apr\0","May\0","Jun\0","Jul\0","Aug\0","Sep\0","Oct\0","Nov\0","Dec\0",
};
#define _RTC_TDIF	+6.0	/* OST = UTC+6.0 */
struct RTCTIME_t rtc;

uint8_t IsLeapYear(uint16_t year)
{
    if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) return 1;
    else return 0;
}

void rtcSetDate(struct  RTCTIME_t* rtc)
{
    uint16_t d;
    uint32_t t;
    t = rtc->ticks;
// seconds
    rtc->sec = t%60;
    t /= 60;
// minutes
    rtc->min = t%60;
    t /= 60;
// hours
    rtc->hour = t%24;
    t /= 24;
// DayOfWeek 01-01-1970 was Thursday :))
    rtc->wday = (t + StartDay)%7;
// year
    rtc->year = StartOfTime;
    while(t >= (365 + IsLeapYear(rtc->year)))
    {
        t -= (365+IsLeapYear(rtc->year));
        ++rtc->year;
    }
// month
    rtc->month = 0;
    d = day_in_month[rtc->month];
    while(t >= d)
    {
        t -= d;
        d = day_in_month[++rtc->month];
        if(rtc->month == 1) d += IsLeapYear(rtc->year);
    }
    rtc->month += 1;
// day
    rtc->mday = t + 1;
}

void rtcSetTime(struct  RTCTIME_t* rtc)
{
    uint32_t t;
    uint16_t y,m;
    t = rtc->sec + rtc->min*(uint32_t)60 +
        rtc->hour*(uint32_t)(60*60) + (rtc->mday - 1)*sec_in_day;
    for(m = 0; m != rtc->month-1; m++)
        t += day_in_month[m]*sec_in_day;
    if(rtc->month > 2) t += sec_in_day*IsLeapYear(rtc->year);
    for(y = StartOfTime; y < rtc->year; y++)
    {
        t += sec_in_year + sec_in_day*IsLeapYear(y);
    }
    rtc->ticks = t;
}
/*
TEST :  Fri, 02 Feb 2018 15:49:30 GMT  1517586570
        Sun, 02 Mar 1980 15:49:30 GMT   320860170
        Thu, 02 Mar 2034 15:49:30 GMT  2024927370
        Fri, 01 Mar 2024 00:00:00 GMT 1709251200
        Thu, 29 Feb 2024 23:59:59 GMT 1709251199

*/
void rtcCalendar(struct  RTCTIME_t* rtc)
{
    printf("\nCurrent Time %s, %02u:%02u:%02u",nDayOfWeek[rtc->wday],rtc->hour,rtc->min,rtc->sec);
    printf(" %u-%s-%u",rtc->mday,Month[rtc->month-1],rtc->year);
    printf(" GMT %u\n\r",rtc->ticks);
}
