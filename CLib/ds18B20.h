#ifndef DS18B20_H_INCLUDED
#define DS18B20_H_INCLUDED

//�������
#define LOCALNUMDS      3   // 3 ������� � ���������
#define	MAXNUMDS		3		//������������ ���������� ������������ ��������
#define	FAMILYCODE	    0x28	//Family Code �������

struct DSROM_t  // ������������� ��������
{
    uint8_t family;       // TARGETFC
    uint8_t rom[6];         // 48 bits contain a unique serial number
    uint8_t cs;           // CRC
};
struct ONEWIRE_t
{
    uint8_t lastROM[8];         // ������ ��� ������ ��������
    uint8_t lastCollision[8];   // ����������� ������
    uint8_t owCrc;              // CRC
    uint8_t owError;             // ��� ������
    uint8_t localNumDS;		    // ����� ������� ��������
    uint8_t totalNumDS;		    // �����  �������� (� ������� ����� � �����������)
    uint8_t localSerial[2];       //�������� ����� ���������
    uint8_t localTPL;          // ��� �����
};
#define dsNORMAL    0x7F
struct SCRATCHPAD_t // ������ ��������
{
// temp = temperature[0] + temperature[1]<<8 = �����������*16
    uint8_t tmp[2];
    uint8_t user1;  // ����� ������� <= 250, ��������������� �������� 75
// ��� �������������� ������� user2 = deflect<<4 + parabolic
// Tcorr = (temp + (int)deflect)/16 -(uint)Parabolic/56250*((temp + (int)deflect)/16 - 35)^2
    uint8_t user2;         //  ��������������� �������� 70 // ����� ����� user1<<8 + user2
    uint8_t config; // 0-����,127-������   ��� ��� ����� = 3-config/32
    uint8_t _ff;            // 0xFF
    uint8_t reserved;       //
    uint8_t _10;            // 0x10
    uint8_t cs;             // CRC
};
//
uint8_t owInit(void);
uint8_t owExit(void);
int16_t owGetTemperature(uint8_t iDS);
uint8_t owGetNumDs(void);
void owPrintTemp(int16_t tmp);
void owMeasureTemp(void);
uint8_t owSearchRom(void);
void owSortSerial(void);
void owCorrectTemp(uint8_t iDS);
uint8_t owCRC(uint8_t *buf,uint8_t length);
uint8_t owReset(void);
void owWrite_byte(uint8_t owByte);
void owReadScratchpad(uint8_t ID);
extern struct SCRATCHPAD_t SCRATCHPAD[MAXNUMDS];
#define ERROR_OK       0
#define ERROR_COMM     1
#define ERROR_CRC      2
#define ERROR_PULL     3
#define ERROR_OTHER    4

#define READ_ROM            0x33
#define MATCH_ROM           0x55
#define SKIP_ROM            0xCC
#define CONVERT             0x44
#define WRITE_SCRATCHPAD    0x4E
#define READ_SCRATCHPAD     0xBE
#define COPY_SCRATCHPAD     0x48
#define SEARCH_ROM          0xF0

#define RES09 ( 0 << 5 )
#define RES10 ( 1 << 5 )
#define RES11 ( 2 << 5 )
#define RES12 ( 3 << 5 )
// ��������� ����������� � ������������ � APPLICATION NOTE 208 � ����������� ��������
// Curve Fitting the Error of a Bandgap-Based Digital Temperature Sensor
// Tk = 100*(Td_DS+Deflect)/16 - 100*Parabolic/56250*((Td+Deflect)/16 - T_ZERO_SLOPE)^2)
// Tk ��������� � ������ , Td - ������ � DS18B20
//void owCorrectTemp(uint8_t iDS)
//{
//#define T_ZERO_SLOPE 35
//    uint8_t Parabolic;
//    int8_t Deflect;
//    uint16_t wrkT;
//    int16_t uT;
//    uT = (int16_t)SCRATCHPAD[iDS].temperature[1]<<8 | SCRATCHPAD[iDS].temperature[0];
//    Deflect = SCRATCHPAD[iDS].user2;
//    Parabolic = Deflect & 0x0F;
//    Deflect /= 16;
//    uT += Deflect;
//    wrkT = uT - 16*T_ZERO_SLOPE;
//    if((int16_t)wrkT < 0) wrkT = -(int16_t)wrkT;
//    wrkT /= 75; // 56250 = 75*75*10
//    wrkT *= wrkT;
//    wrkT *= Parabolic;
//    wrkT = (wrkT+128)>>8;
//    wrkT *= 10;
//    uT = 6*uT + (uT)/4;
//    uT -= wrkT;
//// ���������� ����������������� �����������
//    SCRATCHPAD[iDS].temperature[0] = uT&0x00FF;
//    SCRATCHPAD[iDS].temperature[1] = uT>>8;
//}
//    //������ �����������
//    for (f1=0; f1<nds; f1++)
//    {
//        match_ROM(f1);
//        RM[f1] = *((char *)(tempTK + f1));
//        txbyte_DS18B20(0xBE);//Read Scrathpad
//        for (f2=0; f2<9; f2++)
//        {
//            scrathpad[f2] = rxbyte_DS18B20();
//        }
//        it_DS18B20 = ((unsigned int)scrathpad[1] << 8) + scrathpad[0];
//        if (dowcrc || (scrathpad[4] == 0) || (it_DS18B20 == 0x0550))
//        {
//            tempTK[f1] = -27300;
//        }
//        else
//        {
//            deflect = scrathpad[3];
//            deflect >>= 4;
//            tmp = scrathpad[3] & bin00001111;
//
//            it_DS18B20 += deflect;
//            lt_DS18B20 = it_DS18B20;
//            ld = (lt_DS18B20-35*16);
//            ld *= ld;
//            ld *= tmp;
//            tempTK[f1] = ((float)lt_DS18B20)*6.25 - ((float)ld)*0,00000694;
//        }
//    }

#endif // DS18B20_H_INCLUDED
//int16_t BinColdEndTemperature = DS18_SCRATCHPAD[0] + ((int16_t)DS18_SCRATCHPAD[1]<<8);
//int8_t Deflect = DS18_SCRATCHPAD[3];
//Deflect >>= 4;
//uint8_t Parabolic = DS18_SCRATCHPAD[3]&0x0F;
//BinColdEndTemperature += Deflect;
//temp  = (float) BinColdEndTemperature/16;
//temp -= (((temp-35)*(temp-35)/56250)*Parabolic);


//void crc_check( unsigned char data, unsigned char * crc_byte )
//{
//    unsigned char bit_count;
//    unsigned char tmp = *crc_byte;
//
//    for ( bit_count = 0; bit_count < 8; bit_count++ )
//    {
//        if ( ( data & 0x01 ) ^ ( tmp & 0x01 ) )
//        {
//            tmp = tmp >> 1;
//            tmp = tmp ^ 0x8c;//0b10001100;
//        }
//        else
//        {
//            tmp = tmp >> 1;
//        }
//
//        data = data >> 1;
//    }
//    *crc_byte = tmp;
//}
//uint8_t OWI_ComputeCRC8(uint8_t *data_in, unsigned int number_of_bytes_to_read )
//{
//#define CRC8INIT	0x00
//#define CRC8POLY	0x18              //0X18 = X^8+X^5+X^4+X^0
//    uint8_t	crc;
//    unsigned int	loop_count;
//    uint8_t	bit_counter;
//    uint8_t	data;
//    uint8_t	feedback_bit;
//
//    crc = CRC8INIT;
//
//    for (loop_count = 0; loop_count != number_of_bytes_to_read; loop_count++)
//    {
//        data = data_in[loop_count];
//
//        bit_counter = 8;
//        do
//        {
//            feedback_bit = (crc ^ data) & 0x01;
//            if (feedback_bit==0x01) crc = crc ^ CRC8POLY;
//
//            crc = (crc >> 1) & 0x7F;
//            if (feedback_bit==0x01) crc = crc | 0x80;
//
//            data = data >> 1;
//            bit_counter--;
//        }
//        while (bit_counter > 0);
//    }
//    return crc;
//}
