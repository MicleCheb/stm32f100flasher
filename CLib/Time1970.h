#ifndef TIME1970_H_INCLUDED
#define TIME1970_H_INCLUDED
#include <stdint.h>

struct RTCTIME_t
{
    uint16_t	year;	/* 1970..2106 */
    uint8_t		month;	/* 1..12 */
    uint8_t		mday;	/* 1..31 */
    uint8_t		hour;	/* 0..23 */
    uint8_t		min;	/* 0..59 */
    uint8_t		sec;	/* 0..59 */
    uint8_t		wday;	/* 0..6 (Sun..Sat) */
    uint32_t    ticks;
};

uint8_t IsLeapYear(uint16_t year);
void rtcSetDate(struct  RTCTIME_t* rtc);
void rtcSetTime(struct  RTCTIME_t* rtc);
void rtcCalendar(struct  RTCTIME_t* rtc);
//
extern struct RTCTIME_t rtc;
//

#endif // TIME1970_H_INCLUDED
