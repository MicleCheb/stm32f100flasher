#include "Board.h"
#include "xprintf.h"
#include "ds18B20.h"

//struct FIFO_t TX1_FIFO;
struct FIFO_t RX1_FIFO;
//struct FIFO_t TX2_FIFO;
struct FIFO_t RX2_FIFO;
struct CDC_LINE_CODING_t uartCDC;
uint8_t uart1CMD,uart2CMD;
uint16_t BoardFlags,FlasherFlags,rtcAlarm,LedTimer;
uint8_t BlueLedMask,GreenLedMask,LedMask;
struct
{
    uint8_t cnt;
    uint8_t cmd;
} Button;
#define BUTTON_WAS_PRESSED  Button.cnt&(1<<0)
//RTC_IRQHandler
uint32_t BoardTicks,BoardSeconds;
struct RTCTIME_t rtcTIME;


void RTC_IRQHandler(void)
{
    RTC->CRL = 0;
//    rtcTicks.t16[0] = RTC->CNTL;
//    rtcTicks.t16[1] = RTC->CNTH;
    ++BoardTicks;
    if((BoardTicks % BOARDFREQ) == 0) ++BoardSeconds;
//
    if(rtcAlarm) --rtcAlarm;
    if(LedTimer)
    {

        --LedTimer;
        if(LedTimer == 0) GreenLedMask = MaskOff;
    }
//
    if((BoardTicks % (BOARDFREQ/LEDFREQ)) == 0)
    {
        if(GreenLedMask & LedMask) GREEN_LED_On;
        else GREEN_LED_Off;
        if(BlueLedMask & LedMask ) BLUE_LED_On;
        else BLUE_LED_Off;
        LedMask <<= 1;
        if(LedMask == 0) LedMask = 1;
        CheckButton();
    }
//
    if((BoardTicks&0x000000FF) == 3) BoardFlags |= flT;
}
//
void USART1_IRQHandler(void)
{
    if(USART1->SR & USART_SR_RXNE)  // data ready?
    {
        RX1_FIFO.buf[ RX1_FIFO.Put++ % DATABUFSIZE] = USART1->DR; // save it
    }
}
//
void USART2_IRQHandler(void)
{
    if(USART2->SR & USART_SR_RXNE)  // data ready?
    {
        RX2_FIFO.buf[RX2_FIFO.Put++ % DATABUFSIZE] = USART2->DR; // save it
    }
}
//
void uart1_putc(unsigned char ch)
{
    USART1->DR = ch;
    while(!(USART1->SR & USART_SR_TC));
}
//
unsigned char uart1_getc(void)
{
    while(RX1_FIFO.Put == RX1_FIFO.Get);
    return RX1_FIFO.buf[RX1_FIFO.Get++ % DATABUFSIZE];
}
//
void uart2_putc(unsigned char ch)
{
    USART2->DR = ch;
    while(!(USART2->SR & USART_SR_TC));
}
//
unsigned char uart2_getc(void)
{
    while(RX2_FIFO.Put == RX2_FIFO.Get);
    return RX2_FIFO.buf[RX2_FIFO.Get++ % DATABUFSIZE];
}
//
void initFlasher(void)
{
    RCC->APB2ENR = RCC_APB2ENR_IOPAEN + 1*RCC_APB2ENR_IOPBEN +
                   RCC_APB2ENR_IOPCEN + 0*RCC_APB2ENR_IOPDEN +
                   RCC_APB2ENR_USART1EN + RCC_APB2ENR_AFIOEN;
    /**BUTTON GPIO Configuration
    PA0     ------> BUTTON
    */
    /**USART1 GPIO Configuration
       PA9     ------> USART1_TX ---> PB6
       PA10    ------> USART1_RX ---> PB7
    */
    /**USART2 GPIO Configuration
        PA2     ------> USART2_TX
        PA3     ------> USART2_RX
    */
//    GPIOA->CRH = (4<<(4*(15-8)))|   /* PA15 */
//                 (4<<(4*(14-8)))|   /* PA14 */
//                 (4<<(4*(13-8)))|   /* PA13 */
//                 (4<<(4*(12-8)))|   /* PA12 */
//                 (4<<(4*(11-8)))|   /* PA11 */
//                 (4<<(4*(10-8)))|   /* PA10 RX1 8 REMAP to PB7 */
//                 (4<<(4*( 9-8)))|   /* PA9  TX1 9 REMAP to PB6 */
//                 (4<<(4*( 8-8)));   /* PA8 */
    GPIOA->CRL = (4<<(4*(7)))|      /* PA7 */
                 (4<<(4*(6)))|      /* PA6 */
                 (4<<(4*(5)))|      /* PA5 */
                 (4<<(4*(4)))|      /* PA4 */
                 (8<<(4*(3)))|      /* PA3 RX2 */
                 (9<<(4*(2)))|      /* PA2 TX2 */
                 (4<<(4*(1)))|      /* PA1   */
                 (8<<(4*(0)));      /* PA0 BUTTON */
// PORTB
    /** NONE */
//    GPIOB->CRH = (4<<(4*(15-8)))|   /* PB15 */
//                 (4<<(4*(14-8)))|   /* PB14 */
//                 (4<<(4*(13-8)))|   /* PB13 */
//                 (4<<(4*(12-8)))|   /* PB12 */
//                 (4<<(4*(11-8)))|   /* PB11 */
//                 (4<<(4*(10-8)))|   /* PB10 */
//                 (4<<(4*( 9-8)))|   /* PB9  */
//                 (4<<(4*( 8-8)));   /* PB8 */
    GPIOB->CRL = (8<<(4*(7)))|      /* PB7 RX1 REMAP */
                 (9<<(4*(6)))|      /* PB6 TX1 REMAP */
                 (4<<(4*(5)))|      /* PB5 */
                 (4<<(4*(4)))|      /* PB4 */
                 (4<<(4*(3)))|      /* PB3 */
                 (4<<(4*(2)))|      /* PB2 */
                 (4<<(4*(1)))|      /* PB1 */
                 (4<<(4*(0)));      /* PB0 */
// PORTC
    /**LED GPIO Configuration
        PC8     ------> BLUE_LED
        PC9     ------> GREEN_LED
    */
    GPIOC->CRH = (4<<(4*(15-8)))|   /* PC15 */
                 (4<<(4*(14-8)))|   /* PC14 */
                 (4<<(4*(13-8)))|   /* PC13 */
                 (4<<(4*(12-8)))|   /* PC12 */
                 (4<<(4*(11-8)))|   /* PC11 */
                 (4<<(4*(10-8)))|   /* PC10 */
                 (1<<(4*( 9-8)))|   /* PC9 GREEN_LED */
                 (1<<(4*( 8-8)));   /* PC8 BLUE_LED */
    GPIOC->CRL = (4<<(4*(7)))|      /* PC7 */
                 (4<<(4*(6)))|      /* PC6 */
                 (4<<(4*(5)))|      /* PC5 */
                 (4<<(4*(4)))|      /* PC4 */
                 (4<<(4*(3)))|      /* PC3 */
                 (4<<(4*(2)))|      /* PC2 */
                 (4<<(4*(1)))|      /* PC1 */
                 (4<<(4*(0)));      /* PC0 */
// PORTD
    /** NONE */
//    GPIOD->CRH = (4<<(4*(15-8)))|   /* PD15 */
//                 (4<<(4*(14-8)))|   /* PD14 */
//                 (4<<(4*(13-8)))|   /* PD13 */
//                 (4<<(4*(12-8)))|   /* PD12 */
//                 (4<<(4*(11-8)))|   /* PD11 */
//                 (4<<(4*(10-8)))|   /* PD10 */
//                 (4<<(4*( 9-8)))|   /* PD9  */
//                 (4<<(4*( 8-8)));   /* PD8 */
//    GPIOD->CRL = (4<<(4*(7)))|      /* PD7 */
//                 (4<<(4*(6)))|      /* PD6 */
//                 (4<<(4*(5)))|      /* PD5 */
//                 (4<<(4*(4)))|      /* PD4 */
//                 (4<<(4*(3)))|      /* PD3 */
//                 (4<<(4*(2)))|      /* PD2 */
//                 (4<<(4*(1)))|      /* PD1 */
//                 (4<<(4*(0)));      /* PD0 */
    BLUE_LED_On;
    GREEN_LED_On;
    LedMask = 1;
    GreenLedMask = MaskAll;
    BlueLedMask = MaskAll;
    LedTimer = 255;
//
    if(BUTTON_IS_PRESSED)
    {
        Button.cnt = 0x01; // Короткое нажатие
    }
    else
    {
        Button.cnt = 0;     // Нет нажатий
    };
    Button.cmd = 0;
///
    SystemClock_Config();
/// Init UART1
//    TX1_FIFO.head=TX1_FIFO.Get=0;
    RX1_FIFO.Put=RX1_FIFO.Get=0;
/// REMAP USART1 to PORT B for use with STLink
    AFIO->MAPR = AFIO_MAPR_USART1_REMAP;
    uartCDC.dwDTERate = BAUDRATE;
    uartCDC.bDataBits = 8;
    uartCDC.bParityType = 0;    // None
    uartCDC.bStopBits = 0;      // 1
    USART1->BRR = UARTFREQ/BAUDRATE;
    USART1->CR1 = USART_CR1_RE + USART_CR1_TE + 1*USART_CR1_RXNEIE + 0*USART_CR1_TXEIE+0*USART_CR1_UE;
//    USART1->SR = 0x00C0;
    NVIC_EnableIRQ(USART1_IRQn);
///	USART1->CR1 |= (1 << 13);
///    USART1->CR2 = USART_CR2_STOP;
/// Init UART2
//    TX2_FIFO.head=TX2_FIFO.Get=0;
    RX2_FIFO.Put=RX2_FIFO.Get=0;
    RCC->APB1ENR = RCC_APB1ENR_USART2EN+RCC_APB1ENR_BKPEN+RCC_APB1ENR_PWREN;
    USART2->BRR = UARTFREQ/BAUDRATE;
    USART2->CR1 = USART_CR1_RE + USART_CR1_TE + 1*USART_CR1_RXNEIE + 0*USART_CR1_TXEIE+0*USART_CR1_UE;
//    USART2->SR = 0x00C0;
    NVIC_EnableIRQ(USART2_IRQn);
}
//
void SystemClock_Config(void)
{
    /** Configure system clock generator */
/// После программирования надо сделать RESET или снять питание
    RCC->CR = RCC_CR_HSEON | RCC_CR_HSION; /** Enable HSE */
    while(!(RCC->CR & RCC_CR_HSERDY));
    /** PLL configuration: PLLCLK = HSE/1 * 3 = 24 MHz */
    RCC->CFGR = RCC_CFGR_PLLMULL3 + RCC_CFGR_PLLSRC + RCC_CFGR_SW_PLL;
//    RCC->CFGR2 = RCC_CFGR2_PREDIV1_DIV1;
    RCC->CR |= RCC_CR_PLLON; /** Enable PLL */
    while(!(RCC->CR & RCC_CR_PLLRDY)); /** Wait PLL */
    while(!(RCC->CFGR & RCC_CFGR_SWS_PLL));/** Wait SystemClock Switch */
/// RTC config
    RCC->APB1ENR |= RCC_APB1ENR_PWREN + RCC_APB1ENR_BKPEN; // Enable PWR and BKP clocks
    PWR->CR |= PWR_CR_DBP;  // Allow access to BKP Domain
    RCC->BDCR = RCC_BDCR_BDRST;
    RCC->BDCR = RCC_BDCR_LSEON+RCC_BDCR_RTCSEL_LSE+RCC_BDCR_RTCEN; // start LSE
    while((RCC->BDCR & RCC_BDCR_LSERDY) == 0);
    while (!(RTC->CRL & RTC_CRL_RTOFF));	/* Wait for RTC internal process */
    RTC->CRL = RTC_CRL_CNF;					/* Enter RTC configuration mode */
    RTC->PRLH = 0;
    RTC->PRLL = LSEFREQ/BOARDFREQ - 1;	    /* Set RTC clock divider for 1/64 sec tick */
    RTC->CRH = RTC_CRH_SECIE;
    RTC->CNTH = 0;
    RTC->CNTL = 0;
    BoardTicks = 0;
    RTC->CRL = 0;						    /* Exit RTC configuration mode */
    while (!(RTC->CRL & RTC_CRL_RTOFF));	/* Wait for RTC internal process */
//  while ( !(RTC_CRL & RTC_CRL_RTOFF));      /* Wait for RTC is in sync */
    PWR->CR &= ~PWR_CR_DBP;	/* Inhibit write access to backup domain */
    BoardTicks = 0;
    rtcTIME.year = 2017;
    rtcTIME.month = 7;
    rtcTIME.mday = 3;
    rtcTIME.wday = 1;
    rtcTIME.hour = 21;
    rtcTIME.min = 52;
    rtcTIME.sec = 23;
    rtc_settime(&rtcTIME);
    NVIC_EnableIRQ(RTC_IRQn);
}
//
void SystemInit (void)
{
    /*!< Vector Table base offset field. This value must be a multiple of 0x200. */
#define VECT_TAB_OFFSET  0x0
/// SCB->VTOR = SRAM_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal SRAM. */
    SCB->VTOR = FLASH_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal FLASH. */
}
/*------------------------------------------*/
/* Get time in calendar form                */
/*------------------------------------------*/
const uint8_t samurai[] = {31,28,31,30,31,30,31,31,30,31,30,31};
void rtc_gettime (struct RTCTIME_t* rtc)
{
    uint32_t utc, n, i, d;

//    if (!rtc_getutc(&utc)) return 0;
    utc = BoardSeconds;
    utc += (uint32_t)(_RTC_TDIF * 3600);

    rtc->sec = (uint8_t)(utc % 60);
    utc /= 60;
    rtc->min = (uint8_t)(utc % 60);
    utc /= 60;
    rtc->hour = (uint8_t)(utc % 24);
    utc /= 24;
    rtc->wday = (uint8_t)((utc + 4) % 7);
    rtc->year = (uint16_t)(1970 + utc / 1461 * 4);
    utc %= 1461;
    n = ((utc >= 1096) ? utc - 1 : utc) / 365;
    rtc->year += n;
    utc -= n * 365 + (n > 2 ? 1 : 0);
    for (i = 0; i < 12; i++)
    {
        d = samurai[i];
        if (i == 1 && n == 2) d++;
        if (utc < d) break;
        utc -= d;
    }
    rtc->month = (uint8_t)(1 + i);
    rtc->mday = (uint8_t)(1 + utc);
}

/*------------------------------------------*/
/* Set time in calendar form                */
/*------------------------------------------*/

void rtc_settime (struct  RTCTIME_t* rtc)
{
    uint32_t utc, i, y;
    y = rtc->year - 1970;
    if (y > 2106 || !rtc->month || !rtc->mday) return;
    utc = y / 4 * 1461;
    y %= 4;
    utc += y * 365 + (y > 2 ? 1 : 0);
    for (i = 0; i < 12 && i + 1 < rtc->month; i++)
    {
        utc += samurai[i];
        if (i == 1 && y == 2) utc++;
    }
    utc += rtc->mday - 1;
    utc *= 86400;
    utc += rtc->hour * 3600 + rtc->min * 60 + rtc->sec;
    utc -= (uint32_t)(_RTC_TDIF * 3600);
    BoardSeconds = utc;
}
//
void outCalendar(void)
{
    xprintf("Total : %lu\n",BoardTicks/BOARDFREQ); // BoardSeconds
    rtc_gettime (&rtcTIME);
    xprintf("%u-%u-%u %u %u:%u:%u\n", \
            rtcTIME.year,rtcTIME.month,rtcTIME.mday,rtcTIME.wday, \
            rtcTIME.hour,rtcTIME.min,rtcTIME.sec);

}
///
void ProcessButtonCode(void)
{
    switch(Button.cmd)
    {
    case 0x02:
        BlueLedMask = 0x77;
        break;
    case 0x06:
//        GreenLedMask = 0x55;
        BoardFlags &= ~flUart;
        break;
    case 0x0E:
        GreenLedMask = 0x01;
        BlueLedMask = 0x10;
        break;
    case 0x28: // Reset
        GREEN_LED_On;
        BLUE_LED_On;
        IWDG->KR = 0xCCCC;
        while(1);
        break;
    default:
        break;
    }
}
void CheckButton(void)
{
    if( Button.cnt < 250 ) Button.cnt += 2;
    if(BUTTON_IS_PRESSED)
    {
        if(BUTTON_WAS_PRESSED)  /// check bad press
        {
            if (Button.cnt > (BUTTONFREQ+BUTTONFREQ/2)) Button.cmd=0; // or SpecialEvent
        }
        else    /// event PRESS
        {
            Button.cnt = 1;
        }
    }
    else    /// NOT PRESSED
    {
        if(BUTTON_WAS_PRESSED)  // event
        {
            Button.cmd <<= 1;
            if (Button.cnt <= (BUTTONFREQ/2))
            {
                Button.cmd |= 0x00;
            }
            else if (Button.cnt < (BUTTONFREQ/2+BUTTONFREQ))
            {
                Button.cmd |= 0x01;
            }
            else    /// bad press
            {
                Button.cmd = 0x00;
            }
            Button.cnt = 0;
        }
        else    /// check time-out
        {
            if (Button.cnt == BUTTONFREQ*2)
            {
                ProcessButtonCode();
                Button.cmd=1;
            }

        }
    }
}
//
void setAlarm(uint16_t alarm)
{
    rtcAlarm = alarm;
}
void DelayAlarm(uint16_t ticks)
{
    setAlarm(ticks);
    while(rtcAlarm);
}
//
void delay (uint16_t ticks)
{
    uint16_t j = RTC->DIVL;
    while(ticks--)
    {
        while(j == RTC->DIVL);
        j= RTC->DIVL;
    }
}
//
void printUart(void)
{
    /**USART2 GPIO Configuration
    PA2     ------> USART2_TX
    PA3     ------> USART2_RX
    */
    xprintf("\nUART2 PA2(TX) PA3(RX)>start :");
    xprintf(" %lu",uartCDC.dwDTERate);
    xprintf(" %u",uartCDC.bStopBits);
    xprintf(" %u",uartCDC.bParityType);
    xprintf(" %u",uartCDC.bDataBits);
    xprintf(" %02X\n",uartCDC.bState);
}
//
void UsbUart(void)
{
    uint8_t ch;
    printUart();
    USART2_ENABLE;
    while(BoardFlags & flUart)
    {
// RX2 not empty
        if(RX2_FIFO.Put != RX2_FIFO.Get)
        {
            if(USART1_EMPTY)
            {
                ch = RX2_FIFO.buf[RX2_FIFO.Get++ % DATABUFSIZE];
                USART1_SEND(ch);
            }
        }
// RX1 not empty
        if(RX1_FIFO.Put != RX1_FIFO.Get)
        {
            if(USART2_EMPTY)
            {
                ch = RX1_FIFO.buf[RX1_FIFO.Get++ % DATABUFSIZE];
                if(ch == 0x03) BoardFlags &= ~flUart;
                else USART2_SEND(ch);
            }
        }
    }
//     USART2_DISABLE;
}
//
uint8_t outDS18B20(void)
{

    uint8_t status,nDS,j;
    status=owInit();
    if(status)
    {
        return status+owExit();
    };
    nDS = owGetNumDs();
    xprintf("nDS = %u ",nDS);
    xprintf("T - measure, Q - quit\n");
    while(1)
    {
        switch(uart1_getc())
        {
        case 'T':
            owMeasureTemp();
            for(j=0; j<nDS; j++) owPrintTemp(owGetTemperature(j));
            xprintf("\n");
            break;
        case 'Q':
            return status+owExit();
            break;
        default:
            break;
        }
    }
}
//
uint8_t getDigit(long *digit)
{
    char Addr[64],*pAddr = Addr;
//    long adr;
    xgets(Addr,sizeof(Addr));
    return xatoi(&pAddr, digit);
}
//

void hexprtBuf(uint8_t *buf,uint16_t length)
{
    while(length--) xprintf("%02x",*buf++);
    xprintf("\n");
}
//
