/* Flasher from CC2511 to STM32F100 - STM32F103 */
#include "Board.h"
#include "Flasher.h"
#include "xprintf.h"

int main(void)
{
//    uint32_t j;

//    float x,y,z;
    initFlasher();
    /* Enable UART1 and attach it to xprintf module for console */
//    uart1_init(115200);
    USART1_ENABLE;
    USART2_ENABLE;
    xdev_out(uart1_putc);
    xdev_in(uart1_getc);
    xputs("\n\nSTM32F100 Flasher\n");
//
    BlueLedMask = MaskFind;
    GreenLedMask = MaskOff;
    FlasherFlags=0;
    DelayAlarm(BOARDFREQ*4);
    printHelp();
    while(1)
    {
        BlueLedMask = MaskNorma;
        uart1CMD = uart1_getc();
        switch(uart1CMD)
        {
        case 'B':
            BlueLedMask = MaskFind;
            GreenLedMask = MaskNorma;
            LedTimer= BOARDFREQ*4;
            xprintf("BootLoader: Not implemented\n");
            GreenLedMask=MaskOff;
            printHelp();
            break;
        case 'P':
            BlueLedMask = MaskProg;
            GreenLedMask = MaskFind;
            LedTimer= BOARDFREQ*4;
            xprintf("Flasher...");
            xprintf("Error 0x%02x\n",FlasherMain());
            GreenLedMask=MaskOff;
            printHelp();
            break;
        case 'R':
            xprintf("RF: Not implemented\n");
            printHelp();
            break;
        case 'T':
            xprintf("DS18B20 temperature\n");
            outDS18B20();
            printHelp();
            break;
        case 'H':
            BlueLedMask = MaskNorma; //LedsWait();
            printHelp();
            xprintf("STM32F100 monitor %lu\n",BoardSeconds);
            outCalendar();
            break;
        case 'U':
            GreenLedMask=MaskUART;
            BoardFlags |= flUart;
            UsbUart();
            GreenLedMask=MaskOff;
            printHelp();
            break;
//        case 'Q': /* Soft RESET  quit */
//            break;
        default:
            break;
        }
//        __WFI(); //        goToSleep();
    }
//  Never return here
    return 0;
}
//
void printHelp(void)
{
    xprintf("\nSTM32 USB Flash Loader\n");
    xprintf(" H - Help\n");
    xprintf(" U - USB-RS232 (Ctrl-C end)\n");
    xprintf(" P - MCU FLASH LOADER\n");
    xprintf(" R - USB-RADIO (Ctrl-C end)\n");
    xprintf(" T - DS18B20 (Q end)\n");
//    printf(" S - USB-SPI (Ctrl-C end)\n");
    xprintf(" B - Self BootLoader\n");
//    xprintf(" Q - REBOOT\n");
}


/* ************************** END *********************************** */

