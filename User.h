#ifndef USER_H_INCLUDED
#define USER_H_INCLUDED
#include <stdint.h>

uint8_t UserInit(void);
uint8_t UserUnprotect(void);
uint8_t UserRead(void);
uint8_t UserErase(void);
uint8_t UserWrite(void);
uint8_t UserVerify(void);
uint8_t UserProtect(void);
uint8_t UserExit(void);
//
const uint8_t  UserDesc[]="UserExample TEST";
const struct FlashProg_t  UserE = \
{
    UserDesc,UserInit,UserUnprotect,UserRead, \
    UserErase,UserWrite,UserVerify,UserProtect,UserExit,'0',
};
//
uint8_t UserInit(void)
{
    xprintf("UserInit\n");
    return 0;
}
//
uint8_t UserUnprotect(void)
{
    return 0;
}
//
uint8_t UserRead(void)
{
    xprintf("UserRead\n");
    return 0;
}
//
uint8_t UserErase(void)
{
//    char Addr[64],*pAddr = Addr;
//    long adr;
    long adr;
    xprintf("UserErase at ");
//    xgets(Addr,sizeof(Addr));
//    xatoi(&pAddr, &adr);
    getDigit(&adr);
    xprintf("Addr 0x%08lx\n",adr);
    return 0;
}
//
uint8_t UserWrite(void)
{
    xprintf("UserWrite\n");
    return 0;
}
//
uint8_t UserVerify(void)
{
    return 0;
}
//
uint8_t UserProtect(void)
{
    return 0;
}
//
uint8_t UserExit(void)
{
    xprintf("UserExit\n");
    return 0;
}
//
#endif // USER_H_INCLUDED
