#ifndef FLASHER_H_INCLUDED
#define FLASHER_H_INCLUDED
#include <stdint.h>
// Common Flags
#define SpI (1<<7)  // uart SPI
#define CdC (1<<8)  // CDC mode
#define PrG (1<<9)  // Flash Programming
// JTAG Flags
#define TmS (1<<0)  // SBW TMS state
#define TdI (1<<1)  // SBW TDI bit
#define TdO (1<<2)  // SBW TDO bit
#define TcK (1<<4)  // SBW TCLK state
// SWD Flags
#define B32 (1<<5)  // SWD 32 BIT mode
#define IgN (1<<6)  // SWD ignore ACK
//

#define FLASH_PAGE_SIZE (uint16_t)1024
#define FLASH_SIZE FLASH_PAGE_SIZE*32
extern uint16_t FlasherFlags;
//
struct FlashProg_t
{
    uint8_t *pDescriptor;
    uint8_t (*pfInit)(void);
    uint8_t (*pfUnProtect)(void);
    uint8_t (*pfRead)(void);
    uint8_t (*pfErase)(void);
    uint8_t (*pfWrite)(void);
    uint8_t (*pfVerify)(void);
    uint8_t (*pfProtect)(void);
    uint8_t (*pfExit)(void);
    uint8_t ind;
};
extern const struct FlashProg_t* FlashTable[];

uint8_t FlasherMain(void);
void printFlashHelp(void);
void FlasherHelp(void);
uint8_t Flasher(struct FlashProg_t* FlashFun);
#endif // FLASHER_H_INCLUDED
