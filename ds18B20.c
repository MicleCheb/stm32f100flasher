#include "Board.h"
#include "xprintf.h"
#include "ds18B20.h"

struct DSROM_t DSROM[MAXNUMDS]; // 1 на датчик с номером линии
struct SCRATCHPAD_t SCRATCHPAD[MAXNUMDS];
//struct SCRATCHPAD_t tempScratchpad;
struct ONEWIRE_t OneWire;
// табличный метод вычисления Даллас CRC8
const uint8_t owArray[256] =
{
    0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
    157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
    35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
    190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
    70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
    219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
    101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
    248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
    140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
    17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
    175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
    50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
    202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
    87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
    233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
    116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53
};

uint8_t owCRC(uint8_t *buf,uint8_t length)
{
    uint8_t crc = 0;
    while(length--) crc = owArray[crc^*buf++];
    return crc;
}

#define OW_PORT      GPIOC
#define OW_PIN       0
#define OW_REG       CRL
#define OW_INPUT     {OW_PORT->OW_REG&=~(15<<(4*(OW_PIN)));OW_PORT->OW_REG|=(8<<(4*(OW_PIN)));}
#define OW_OUTPUT    {OW_PORT->OW_REG&=~(15<<(4*(OW_PIN)));OW_PORT->OW_REG|=(2<<(4*(OW_PIN)));}
#define OW_HIGH      (OW_PORT->BSRR=(1<<OW_PIN))
#define OW_LOW       (OW_PORT->BSRR=(1<<(OW_PIN+16)))
#define OW_DATA      (OW_PORT->IDR&(1<<OW_PIN))

#define owPULSE     { OW_LOW; OW_OUTPUT; }
#define owRELEASE   { OW_HIGH; OW_INPUT; }
#define owDATA       (OW_DATA)
#define delayMUL    ( (SYSTEMFREQ/1000000) )
#define delay5us   (5*delayMUL-1)
#define delay15us  (15*delayMUL-1)
#define delay45us  (45*delayMUL-1)
#define delay60us  (60*delayMUL-1)
#define delay480us (480*delayMUL-1)

// задержка  микросекунд
void owDelay(uint32_t delay)
{
//    while(delay--)
//    {
//        __NOP();
//        __NOP();
//    };
//    RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;
//    TIM6->PSC = 0;
    TIM6->ARR = delay;
    TIM6->CNT=0;
    TIM6->CR1 = TIM_CR1_OPM|TIM_CR1_CEN;
    while(TIM6->CR1 & TIM_CR1_CEN);
//    T2PR  = 1;
//    T2CTL = 0;  // div64
//    T2CT  = delay; // 13MHz*5usec/64 = 1
//    while(!(T2CTL&(1<<6)));
}
//
uint8_t owInit(void)
{
    RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;
    TIM6->PSC = 0;
    OW_HIGH;
    OW_INPUT;
    if(owReset()) return 1;
    if(owSearchRom() == 0) return 2;
    OneWire.localNumDS = OneWire.totalNumDS;
    owMeasureTemp();
    return 0;
}
//
uint8_t owExit(void)
{
    RCC->APB1ENR &= ~RCC_APB1ENR_TIM6EN;
    OW_INPUT;
    OW_LOW;
    return 0;
}
// сброс устройств
uint8_t owReset(void)
{
    uint8_t noOneWire; // 0 - есть устройства 1 - нет устройств
//    PULSE_HIGH;
//    PULSE_OUTPUT;
//    owDelay(delay15us);
    owPULSE;
    owDelay(delay480us+delay60us);
    owRELEASE;
    owDelay(delay15us);
    owDelay(delay60us);
    noOneWire = owDATA;
    owDelay(delay480us);
    return noOneWire;
}
// запись бита
void owWrite_bit(uint8_t owBit)
{
    owPULSE;
    owDelay(delay5us);
    if(owBit) owRELEASE;
    owDelay(delay60us);
    owRELEASE;
    owDelay(delay5us);
}
// чтение бита
uint8_t owRead_bit(void)
{
    uint8_t owBit=0;
    owPULSE;
    owDelay(delay5us);
    owRELEASE;
    owDelay(delay5us);
    if(owDATA) owBit = 1;
    owDelay(delay60us);
    return owBit;
}
// запись 1 байта в однопроводную линию и подсчёт CRC
void owWrite_byte(uint8_t owByte)
{
    uint8_t i;
    OneWire.owCrc = owArray[OneWire.owCrc^owByte];
    for(i=8; i; i--)
    {
        owWrite_bit(owByte & 1);
        owByte >>= 1;
    }
}
// чтение 1 байта из однопроводной линии и подсчёт CRC
uint8_t owRead_byte(void)
{
    uint8_t i;
    uint8_t owByte=0;
    for(i=8; i; i--)
    {
        owByte >>= 1;
        if(owRead_bit()) owByte |= 0x80;
    }
    OneWire.owCrc = owArray[OneWire.owCrc^owByte];
    return owByte;
}
//
void owMatchRom(uint8_t ID)
{
    uint8_t i;
    uint8_t *buf = (uint8_t *)&DSROM[ID];
    owReset();
    owWrite_byte(MATCH_ROM);
    OneWire.owCrc = 0;
    for (i=0; i < sizeof(struct DSROM_t); i++) owWrite_byte(*buf++);
//    owWrite_byte(FAMILYCODE);
//    for (i=0; i < sizeof(DSROM[0].rom); i++) owWrite_byte(*buf++);
//    owWrite_byte(OneWire.owCrc);
}
void owReadScratchpad(uint8_t ID)
{
    uint8_t i;
    uint8_t *buf =(uint8_t *)&SCRATCHPAD[ID];
    owWrite_byte(READ_SCRATCHPAD);
    OneWire.owCrc = 0;
    for (i=0; i < sizeof(struct SCRATCHPAD_t); i++) *buf++ = owRead_byte();
//    hexprtBuf((uint8_t *)DSROM[index],sizeof(struct SCRATCHPAD_t));
}
// переброс ROM dst <--> src
void owSwapRom(uint8_t dst,uint8_t src)
{
    memcpy((uint8_t *)&OneWire.lastROM,(uint8_t *)&DSROM[dst],sizeof(struct DSROM_t));
    memcpy((uint8_t *)&DSROM[dst],(uint8_t *)&DSROM[src],sizeof(struct DSROM_t));
    memcpy((uint8_t *)&DSROM[src],(uint8_t *)&OneWire.lastROM,sizeof(struct DSROM_t));
}
// сортировка счётчиков по возрастающим номерам
void owSortRom(uint8_t ID)
{
    uint8_t minval,minpos,saveid,serial;
//    for(i=0; i < OneWire.localNumDS; i++) printf("%u ",SCRATCHPAD[i].user1);
//    printf("\n");
    saveid = minpos = ID;
    minval = SCRATCHPAD[minpos].user1;
    while(++ID < OneWire.totalNumDS)
    {
        if(SCRATCHPAD[ID].user1 < minval)
        {
            minval = SCRATCHPAD[ID].user1;
            minpos = ID;
        }
    }
    if(saveid != minpos)
    {
        owSwapRom(saveid,minpos);
        serial = SCRATCHPAD[saveid].user1;
        SCRATCHPAD[saveid].user1 = SCRATCHPAD[minpos].user1;
        SCRATCHPAD[minpos].user1 = serial;
    }
}
// Сортировка датчиков по номерам
void owSortSerial(void)
{
    uint8_t numID;
//    for(numID=0; numID < MAXNUMDS; numID++ ) memset((uint8_t *)&SCRATCHPAD[numID],0,sizeof(struct SCRATCHPAD_t));
    for(numID=0; numID < OneWire.totalNumDS; numID++)
    {
// читаем номера счётчиков и сохраняем по порядку
        owMatchRom(numID);
        owReadScratchpad(numID);
//
        xprintf("RID %u ",numID);
        hexprtBuf((uint8_t *)&SCRATCHPAD[numID],sizeof(struct SCRATCHPAD_t));
        if(OneWire.owCrc == 0) ++OneWire.localNumDS;
//
//        if((SCRATCHPAD[numID].config == 0) || OneWire.owCrc)
//        {
//            SCRATCHPAD[numID].user1 = 0xFF; // Бракованный датчик
//        }
//        else
//        {
//            ++OneWire.localNumDS;
//            if(SCRATCHPAD[numID].config == 0x7F) //Обычный датчик
//            {
//                if(SCRATCHPAD[numID].user1 <= 250)
//                {
//                    ++OneWire.localNumDS;
//                };
//            }
//            else //Датчик с номером линии
//            {
//                OneWire.localSerial[0] = SCRATCHPAD[numID].user1;
//                OneWire.localSerial[0] = SCRATCHPAD[numID].user2;
//                OneWire.localTPL = 3-(SCRATCHPAD[numID].config>>5);
//                SCRATCHPAD[numID].user1 = 0xFE; // Для сортировки
////                printf("TL%u TK%05u\n",localTPL,localSerial);
//            }
    }
//    }
////    printf("FOUND %u\n",OneWire.localNumDS);
//// Сортировка по номерам
////    for(numID=0; numID < OneWire.localNumDS; numID++)
////    {
////        printf("%u ",SCRATCHPAD[numID].user1);
////    }
//    for(numID=0; numID < OneWire.totalNumDS; numID++) owSortRom(numID);
//// для тестовой термокосы
//    if(OneWire.totalNumDS == OneWire.localNumDS )   // нет датчиков с номером линии
//    {
//        numID = --OneWire.localNumDS; // запоминаем в последний датчик номер линии
//        owMatchRom(numID);
//        owReadScratchpad(numID);
//        SCRATCHPAD[numID].config &= ~((1<<6)|(1<<5));
//        owMatchRom(numID);
//        owWrite_byte(WRITE_SCRATCHPAD);
//        owWrite_byte(SCRATCHPAD[numID].user1);
//        owWrite_byte(SCRATCHPAD[numID].user2);
//        owWrite_byte(SCRATCHPAD[numID].config);
//        owMatchRom(numID);
//        owWrite_byte(COPY_SCRATCHPAD);
//    }
////
//    for(numID=0; numID < OneWire.totalNumDS; numID++)
//    {
//        owMatchRom(numID);
//        owReadScratchpad(numID);
//        printf("SID %u ",numID);
//        hexprtBuf((uint8_t *)&SCRATCHPAD[numID],sizeof(struct SCRATCHPAD_t));
//    };
}

// Определяет количество устройств на шине и запоминает их адреса
uint8_t owSearchRom(void)
{
#define ROMBITS 64
    uint8_t owBit,owByte;
    uint8_t bitPosition,pos,i;
    uint8_t FlagDone;
// обнуление данных
    memset((uint8_t *)&DSROM,0,sizeof(DSROM));
    memset((uint8_t *)&OneWire,0,sizeof(OneWire));
    memset((uint8_t *)&SCRATCHPAD,0,sizeof(SCRATCHPAD));

    xprintf("SEARCH ");
    do
    {
        if(owReset())
        {
            OneWire.owError = ERROR_PULL;
            xprintf("ERROR 0\n");
            return 0;   // нет датчиков
        }
//
        owWrite_byte(SEARCH_ROM);
        owByte = 0;
        for(bitPosition=0; bitPosition<ROMBITS; bitPosition++)
        {
            owByte >>= 1;
            owBit = 0;
            if(owRead_bit()) owBit  = 2; // read true bit
            if(owRead_bit()) owBit |= 1; // read complement bit
            switch (owBit)
            {
            case 0x00: // Коллизия 0 и 1
// Проверка предыдущих коллизий
                for(pos=ROMBITS-1; pos>bitPosition; pos--)
                {
                    if(OneWire.lastCollision[pos>>3] & (1<<(pos%8))) break;
                }
                if(OneWire.lastCollision[bitPosition>>3] & (1<<(bitPosition%8))) owBit = 1;
// Есть более ранние коллизии , убираем
                if(pos >  bitPosition)
                {
                    owBit = !owBit;
                }
// Текущая коллизия, запоминаем
                else
                {
                    (OneWire.lastCollision[bitPosition>>3] ^= (1<<(bitPosition%8)));
                }
                break;
            case 0x01: // у всех 0
                owBit = 0;
                break;
            case 0x02: // у всех 1
                owBit = 1;
                break;
            case 0x03: // Нет устройств, ошибка
            default:
                xprintf("ERROR 1 %u\n",bitPosition);
                return 0; // ERROR
                break;
            };
            owWrite_bit(owBit);
            if(owBit) owByte |= 0x80;
            if((bitPosition%8) == 7)
            {
                OneWire.lastROM[bitPosition>>3] = owByte;
                owByte = 0;
            }
        }
// check family crc
// copy rom
        memcpy((uint8_t *)&DSROM[OneWire.totalNumDS],(uint8_t *)&OneWire.lastROM,sizeof(struct DSROM_t));
        ++OneWire.totalNumDS;
// проверка окончания прохода
        FlagDone = 0;
        for (i=0; i<8; i++) if(OneWire.lastCollision[i]) ++FlagDone;
//        printf("Collision %u\n",FlagDone);
    }
    while ((OneWire.totalNumDS < (MAXNUMDS)) && (FlagDone != 0));
    xprintf("TOTAL %u\n",OneWire.totalNumDS);
    OneWire.localNumDS = OneWire.totalNumDS;
    return OneWire.totalNumDS;
}
// измерение температуры со всех датчиков
void owMeasureTemp(void)
{
#define TicksTimer  rtcAlarm
#define goToSleep() __WFI()
    uint8_t iDS;
//    uint8_t checkSerial;
    if(OneWire.localNumDS == 0)
    {
//        sbit(BoardFlags,flSearch); // нет датчиков, искать
        xprintf("NO DS\n");
        return;
    }
// запуск измерения на 1 секунду
    TicksTimer = BOARDSEC*1;
    owReset();
    owWrite_byte(SKIP_ROM);
    owWrite_byte(CONVERT);
// ожидание готовности
    while(owDATA == 0)
    {
        goToSleep();
        if(TicksTimer == 0)
        {
            OneWire.owError = ERROR_PULL;
            xprintf("DQ ERR\n");
            return;
        }
    };
// чтение и вывод температуры
    OneWire.owError = ERROR_OK;
    for(iDS=0; iDS<OneWire.localNumDS; iDS++)
    {
// выбор датчика
        owMatchRom(iDS);
// чтение данных , запоминаем старый серийный номер
//        checkSerial = SCRATCHPAD[iDS].user1;
        owReadScratchpad(iDS);
//      проверка правильности измерения
//        if((SCRATCHPAD[iDS].user1 != checkSerial) || (OneWire.owCrc != 0))
        if((OneWire.owCrc != 0))
        {
//            sbit(BoardFlags,flSearch);
            OneWire.owError = ERROR_CRC;
            SCRATCHPAD[iDS].tmp[0] = 0xF0;  //(-273*16)&0x00FF;
            SCRATCHPAD[iDS].tmp[1] = 0xEE;  //(-273*16)>>8;
//            xprintf("ERR %u\n",OneWire.owError);
        }
//        owPrintTemp((int16_t)SCRATCHPAD[iDS].tmp[1]<<8 | SCRATCHPAD[iDS].tmp[0]);
// для отладки
//        if(iDS == 0) owPrintTemp((int16_t)SCRATCHPAD[iDS].temperature[1]<<8 | SCRATCHPAD[iDS].temperature[0]);
// коррекция температуры
//        owCorrectTemp(iDS);
    }
}
void owPrintTemp(int16_t tmp)
{
    uint8_t j;
    xprintf("T = " );
    if(tmp < 0)
    {
        xprintf("-");
        tmp = -tmp;
    };
    j=tmp>>4;
    xprintf("%u.",j);
    tmp = tmp&0x000F;
    tmp *= 100;
    tmp >>= 4;
    xprintf("%02u ",tmp);
}
int16_t owGetTemperature(uint8_t iDS)
{
    return ((int16_t)SCRATCHPAD[iDS].tmp[1]<<8 | SCRATCHPAD[iDS].tmp[0]);
}
uint8_t owGetNumDs(void)
{
    return OneWire.totalNumDS;
}
