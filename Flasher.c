#include "Board.h"
#include "xprintf.h"
#include "Flasher.h"
#include "User.h"
//
const struct FlashProg_t* FlashTable[]= {&UserE,&UserE,};
uint16_t FlasherFlags;
//struct FlashProg_t* FlashFun;
uint8_t  FlasherMain(void)
{
    uint8_t i,Tind;
    struct FlashProg_t* FlashFun;
    printFlashHelp();
    while(1)
    {
        Tind = uart1_getc();
        switch(Tind)
        {
        case 'Q':
            xprintf("QUIT\n");
            return 0;
            break;
        case 'H':
            printFlashHelp();
            break;
        default:
//
            for(i=0; i < sizeof(FlashTable)/sizeof(struct FlashProg_t*); i++)
            {
                FlashFun = (struct FlashProg_t *)FlashTable[i];
                if(Tind == FlashFun->ind)
                {
                    return Flasher(FlashFun);
                }
            }
            break;
        }
    }
    return 0;
}
//
void printFlashHelp(void)
{
    uint8_t i;
    struct FlashProg_t* FlashFun;
//    FlashFun = (struct FlashProg_t *)FlashTable;
    xprintf("\n\r  H - HELP\n");
    for(i=0; i < sizeof(FlashTable)/sizeof(struct FlashProg_t*); i++)
    {
        FlashFun = (struct FlashProg_t *)FlashTable[i];
        xprintf("%c - %s\n",FlashFun->ind,FlashFun->pDescriptor);
    }
    xprintf("  Q - QUIT\n");
}
//
void FlasherHelp(void)
{
    xprintf(" h - Help\n");
    xprintf(" u - UnProtect\n");
    xprintf(" r - Read\n");
    xprintf(" e - Erase\n");
    xprintf(" w - Write\n");
    xprintf(" v - Verify\n");
    xprintf(" p - Protect\n");
    xprintf(" q - Quit\n");
}
//
uint8_t Flasher(struct FlashProg_t* FlashFun)
{
    uint8_t Tind;
    if(FlashFun->pfInit()) return (0x10 + FlashFun->pfExit());
    FlasherHelp();
    while(1)
    {
        Tind = uart1_getc();
        switch(Tind)
        {
        case 'e':
//            xprintf("Erase...\n");
            if(FlashFun->pfErase()) return (0x40 + FlashFun->pfExit());
            break;
        case 'w':
            xprintf("Prog...\n");
            if(FlashFun->pfWrite()) return (0x50 + FlashFun->pfExit());
//            OsParam.FlashAddr.A32=0;
//            do
//            {
//                OsParam.flashBuf=(__xdata uint8_t *)XRDATA;
//                if(IntelHex())
//                {
//                    printf("HEX ERROR\n");
//                    return (0x40+(OsParam.FlashMemFunc)->flExit());
//                }
//                else
//                {
//                    OsParam.flashBuf=(__xdata uint8_t *)XRDATA;
//                    OsParam.flashCnt =OsParam.flashBuf[0];
//                    switch(OsParam.flashBuf[3])
//                    {
//                    case recEOF:
//                        printf("OK\nVerify Flash\n");
//                        goto FlashOK;
//                        break;
//                    case recData:
//                        OsParam.FlashAddr.A8[0]=OsParam.flashBuf[2];
//                        OsParam.FlashAddr.A8[1]=OsParam.flashBuf[1];
//                        OsParam.flashBuf=(__xdata uint8_t *)XRDATA+4;
//                        if((OsParam.FlashMemFunc)->flWrite()) return (0x40+(OsParam.FlashMemFunc)->flExit());
//                        break;
//                    case   recSegment:
////#define recSegment  2   //:020000021200EA
////                flashAddr.A8[0]=flashBuf[5];
////                flashAddr.A8[1]=flashBuf[4];
////                flashAddr.A8[2]=0;//flashBuf[2];
////                flashAddr.A8[3]=0;//flashBuf[1];
////                flashAddr.A32<<=4;
//                        break;
//                    case recStartSeg:
//// #define recStartSeg 0x03 // :040000030000C00039 Start Segment Address Record.
////                        flashAddr.A8[0]=flashBuf[7];
////                        flashAddr.A8[1]=flashBuf[6];
////                        flashAddr.A8[2]=flashBuf[5];
////                        flashAddr.A8[3]=flashBuf[4];
//                        break;
//                    case recLinear:
////#define recLinear   4   //:02000004FFFFFC
//                        OsParam.FlashAddr.A8[0]=0;//flashBuf[2];
//                        OsParam.FlashAddr.A8[1]=0;//flashBuf[1];
//                        OsParam.FlashAddr.A8[2]=OsParam.flashBuf[5];
//                        OsParam.FlashAddr.A8[3]=OsParam.flashBuf[4];
//                        break;
//                    case recMDKARM:
////#define recMDKARM   5   //:04000005000000CD2A
////                flashAddr.A8[0]=flashBuf[7];
////                flashAddr.A8[1]=flashBuf[6];
////                flashAddr.A8[2]=flashBuf[5];
////                flashAddr.A8[3]=flashBuf[4];
//                        break;
//                    default:
//                        printf("UNKNOWN %02X\n",OsParam.flashBuf[3]);
//                        return (0x50+(OsParam.FlashMemFunc)->flExit());
//                        break;
//                    }
//                };
//            }
//            while(1);
            break;
        case 'r':
            xprintf("Read Adr...\n");

//            OsParam.FlashAddr.A32=getDigit();
//            printf("0x%08lX\n",OsParam.FlashAddr.A32);
//            OsParam.flashBuf=(__xdata uint8_t *)XRDATA;
//// read max 1024 bytes (flashCnt)
            if( FlashFun->pfRead()) return (0x30 + FlashFun->pfExit());
//            OsParam.flashBuf=(__xdata uint8_t *)XRDATA;
//            printf("@%08lX\n",OsParam.FlashAddr.A32);
//            while(OsParam.flashCnt--)
//            {
//                printf("%02X",*OsParam.flashBuf++);
//                if(OsParam.flashCnt%32 == 0)  printf("\n");
//            }
//            printf("\n\r");
            xprintf("OK\n");
            break;
        case 'v':
            FlashFun->pfVerify();
            if(FlashFun->pfVerify()) return (0x60 + FlashFun->pfExit());
            break;
        case 'p':
            if(FlashFun->pfProtect()) return (0x70 + FlashFun->pfExit());
            break;
        case 'u':
            if(FlashFun->pfUnProtect()) return (0x20 + FlashFun->pfExit());
            break;
        case 'q':
            return (0x00 + FlashFun->pfExit());
            break;
        case 'h':
//FlashOK:
            FlasherHelp();
            break;
        default:
            break;
        }
    };
    //    return 0;
}

