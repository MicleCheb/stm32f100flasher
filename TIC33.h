#ifndef TIC33_H_INCLUDED
#define TIC33_H_INCLUDED
//#include <stdint.h>
//typedef unsigned char           uint8_t;
//typedef unsigned short int      uint16_t;
//typedef unsigned long int       uint32_t;
#inline void SetTIC33(void);
void WriteCharTIC33(uint8_t ch);
void WriteBufTIC33(void);
uint8_t DecodeTIC33(uint8_t ch,uint8_t point);
void PrintDec(void);
void PrintHex(void);
void PrintLiter(void);
void PrintStr(uint8_t *str);
void PrintChar(uint8_t ch,uint8_t Ipos);
//Вывод одного разряда для TIC33:
//Для управления используются следующие выводы:
//
// 1    VDD — питание, в зависимости от индикатора может быть от 2.5 до 5.0В
// 2   GND — общий, куда же без него.
// 3   Load — вход защелки данных, защелкивание идет тоже положительном фронтом.
// 4   Din — вход данных.
// 5   Lclk — вход обновления индикации. Присутствует если в контроллере отключен встроенный генератор. На вход нужно подавать сигнал с частотой 50-120Гц.
// 6   Dclk — вход тактов данных, данные проталкиваются по фронту положительного сигнала.
//
//    Ben — разрешение мигания. На моих индикаторах его не было.
//    Bclk — тактовая частота для мигания.

//
//Длительность тактовых импульсов должна быть не меньше 0.4us.
#define TIC_LOAD    (1<<4)  // PORTA
#define TIC_DIN     (1<<5)  // PORTC
#define TIC_LCLK    (1<<4)
#define TIC_DCLK    (1<<3)


#define   TIC33   LATC
#define   Load    LATA4
#define   Din     LATC5
#define   Lclk    LATC4
#define   Dclk    LATC3
#define   SetDin  TIC33.Din=1       // Установка бита данных в 1   TIC33 |= 1<<Din
#define   ClrDin  TIC33.Din=0       // Установка бита данных в 0  TIC33 &=~ (1<<Din)
#define   SetDclk TIC33.Dclk=1      // Установка строба данных в 1 TIC33 |= 1<<Dclk
#define   ClrDclk TIC33.Dclk=0      // Установка строба данных в 0 TIC33 &=~ (1<<Dclk)
#define   SClk    SetDclk;ClrDclk       // Строб данных
#define   SetLoad LATA.Load=1      // Установка строба загрузки в 1 TIC33 |= 1<<Load
#define   ClrLoad LATA.Load=0      // Установка строба загрузки в 0 TIC33 &=~ (1<<Load)
#define   SLoad   SetLoad;ClrLoad       // Строб загрузки
#define   LclkTIC33()    TIC33.Lclk=~TIC33.Lclk
// Вывод на TIC33
// point = 1, вывод точки
// 0...9,-,Градус,Space
//#define   minus   10
//#define   degress 11
//#define   space   12

struct TIC33_t
{
    uint8_t Asimbol;
    uint8_t Esignal;
};

const struct TIC33_t DigitTIC33[]  =
{
    {'0',0xFA},  //0
    {'1',0xA },   //1
    {'2',0xB6},  //2
    {'3',0x9E},  //3
    {'4',0x4E},  //4
    {'5',0xDC},  //5
    {'6',0xFC},  //6
    {'7',0x8A},  //7
    {'8',0xFE},  //8
    {'9',0xDE},  //9
    {'A',32+64+128+2+4+8},  //A
    {'B',64+32+16+8+4},  //B
    {'C',128+64+32+16},  //C
    {'D',2+4+8+16+32},  //D
    {'E',128+64+4+32+16},  //E
    {'F',128+64+4+32},  //F
    {'-',0x04},  // minus
    {'g',2+4+64+128}, // degree
    {' ',0},      // space
    {'.',1},
    {'H',64+32+4+2+8},
    {'L',64+32+16},
    {'O',0xFA},
    {'S',0xDC},
    {'G',128+64+32+16+8},
    {'P',128+64+32+4+2},
    {'U',16+64+32+8+2},
    {'R',2+4+8+32+64+128},
    {'_',16},  //
//
};
uint8_t LCDBuf[9];
//
//void LclkTIC33(void)
//{
//    fbit(TIC33,Lclk);
//}
//
#inline
void SetTIC33(void)
{
    uint8_t i;
//    TRISC = ~(TIC_DIN +TIC_LCLK+TIC_DCLK);
//    TRISA &= ~TIC_LOAD;
//    PORTC=0;
    for(i=0; i<9; i++) LCDBuf[i]=DecodeTIC33('8','.');
    WriteBufTIC33();
}
//
uint8_t DecodeTIC33(uint8_t ch,uint8_t point)
{
    uint8_t j,TIC33s;
    TIC33s=0;
    for(j=0; j < sizeof(DigitTIC33)/sizeof(struct TIC33_t); j++)
    {
        if(ch==DigitTIC33[j].Asimbol)
        {
            TIC33s=DigitTIC33[j].Esignal;
            break;
        }
    }
    if(point) TIC33s |= 1;
    return TIC33s;
}
//
void WriteCharTIC33(uint8_t ch)
{
    uint8_t i;
// Вывод символа
    for(i=8; i; i--)
    {
        if(ch & 128) SetDin;
        else ClrDin;
        SClk;
        ch=ch<<1;
    }
}
//
void WriteBufTIC33(void)
{
//#define i wrk16.u8[0]
    uint8_t i,j;
    j=8;
    for(i=0; i<9; i++) WriteCharTIC33(LCDBuf[j--]);
    SLoad;
}
void PrintHex(void)
{
//    uint8_t i,ch;
//    i=0;
//    do
//    {
//        ch=WRK32.t32&0x0F;
//        if(ch>9) ch += 'A'-10;
//        else ch += '0';
//        LCDBuf[i]=DecodeTIC33(ch,0);
//        WRK32.t32 >>= 4;
//        ++i;
//    }
//    while(WRK32.t32 && (i<9)); //
}
void PrintDec(void)
{
//    uint8_t i;
//    i=0;
//    do
//    {
//        LCDBuf[i]=DecodeTIC33((WRK32.t32%10) + '0',0);
//        WRK32.t32 /= 10;
//        ++i;
//    }
//    while(i < 4);
//    LCDBuf[3] += 1; // point
//    while(WRK32.t32 && (i<9))
//    {
//        LCDBuf[i]=DecodeTIC33((WRK32.t32%10) + '0',0);
//        WRK32.t32 /= 10;
//        ++i;
//    }
}
//void PrintStr(uint8_t *str)
//{
//    uint8_t Ipos=8;
//    while (*str)
//    {
//        LCDBuf[Ipos]=DecodeTIC33(*str,0);
//        ++str;
//        if(*str == '.') LCDBuf[Ipos] |= 1;
//        else --Ipos;
//    }
//}
void PrintChar(uint8_t ch,uint8_t Ipos)
{
    LCDBuf[Ipos]=DecodeTIC33(ch,0);
}
void PrintLiter(void)
{
    uint8_t i;
    i=0;
    do
    {
        LCDBuf[i]=DecodeTIC33( VolumeTotal[i] + '0',0);
        ++i;
    }
    while(i < 4);
    LCDBuf[3] += 1; // point
//    i=5;
    while(VolumeTotal[i] && (i<7))
    {
        LCDBuf[i]=DecodeTIC33( VolumeTotal[i] + '0',0);
        ++i;
    }
}
#endif // TIC33_H_INCLUDED
